import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateLectorDto } from './dto/create-lector.dto';
import { LectorsService } from '../../services/lectors/lectors.service';
import { LectorsCoursesService } from '../../services/lectors/lectors-courses.service';
import { SortLectorDto } from './dto/sort-lector.dto';
import { UpdateLectorDto } from './dto/update-lector.dto';

@Injectable()
export class LectorsControllerService {
  constructor(
    private readonly lectorsService: LectorsService,
    private readonly lectorsCoursesService: LectorsCoursesService,
  ) {}
  create(createLectorDto: CreateLectorDto) {
    return this.lectorsService.createLector(createLectorDto);
  }

  findAllLectors() {
    return this.lectorsService.findAllLectors();
  }

  findAllSort(sortLectorDto: SortLectorDto) {
    return this.lectorsService.findAllSort(sortLectorDto);
  }

  findLectorById(id: string) {
    return this.lectorsService.findLectorById(id);
  }

  updateLectorById(id: string, updateLecrorDto: UpdateLectorDto) {
    return this.lectorsService.updateLector(id, updateLecrorDto);
  }

  removeLector(id: string) {
    return this.lectorsService.removeLector(id);
  }

  addLectorToCourse(lectorId: string, courseId: string) {
    return this.lectorsCoursesService.addLectorToCourse(lectorId, courseId);
  }

  public async findCurrentLector(id: string) {
    const lector = await this.lectorsService.findById(id);
    if (lector) {
      delete lector.password;
    }
    if (!lector) {
      throw new NotFoundException(`Lector is not found`);
    }
    return lector;
  }
}
