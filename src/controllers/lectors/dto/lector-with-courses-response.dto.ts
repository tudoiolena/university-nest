import { IsArray, IsNotEmpty, IsOptional } from 'class-validator';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { LectorResponseDto } from './lector-response.dto';
import { Course } from 'src/services/courses/entities/course.entity';

export class LectorWithCoursesResponseDto extends LectorResponseDto {
  @IsNotEmpty()
  @IsArray()
  @ApiPropertyOptional()
  @IsOptional()
  courses?: Course[];
}
