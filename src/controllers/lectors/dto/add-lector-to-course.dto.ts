import { IsNotEmpty, IsString, IsUUID } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class AddLectorToCourseDto {
  @IsNotEmpty()
  @IsString()
  @IsUUID()
  @ApiProperty()
  courseId: string;
}
