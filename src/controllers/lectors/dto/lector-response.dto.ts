import { IsDate, IsEmail, IsNotEmpty, IsString, IsUUID } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class LectorResponseDto {
  @IsNotEmpty()
  @IsString()
  @IsUUID()
  @ApiProperty()
  id: string;

  @IsNotEmpty()
  @IsDate()
  @ApiProperty()
  createdAt: Date;

  @IsNotEmpty()
  @IsDate()
  @ApiProperty()
  updatedAt: Date;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  name: string;

  @IsNotEmpty()
  @IsEmail()
  @ApiProperty()
  email: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  password: string;
}
