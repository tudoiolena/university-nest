import { IsEmail, IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class LectorEmailDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  @IsEmail()
  email: string;
}
