import { Module } from '@nestjs/common';
import { LectorsModule } from '../../services/lectors/lectors.module';
import { LectorsController } from './lectors.controller';
import { LectorsControllerService } from './lectors.controller.service';
import { LectorsCoursesModule } from '../../services/lectors/lectors-courses.module';

@Module({
  imports: [LectorsModule, LectorsCoursesModule],
  controllers: [LectorsController],
  providers: [LectorsControllerService],
})
export class LectorsControllerModule {}
