import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpStatus,
  HttpCode,
  UseGuards,
  Query,
} from '@nestjs/common';
import { LectorsControllerService } from './lectors.controller.service';
import { CreateLectorDto } from './dto/create-lector.dto';
import { AddLectorToCourseDto } from './dto/add-lector-to-course.dto';
import { LectorIdDto } from './dto/lector-id.dto';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiQuery,
  ApiResponse,
} from '@nestjs/swagger';
import { LectorResponseDto } from 'src/controllers/lectors/dto/lector-response.dto';
import { LectorWithCoursesResponseDto } from './dto/lector-with-courses-response.dto';
import { AuthGuard } from '../../services/auth/guards/auth.guard';
import { CurrentUser } from 'src/services/auth/decorators/current.user.decorator';
import { SortLectorDto } from './dto/sort-lector.dto';
import { UpdateLectorDto } from './dto/update-lector.dto';

@UseGuards(AuthGuard)
@Controller('lectors')
export class LectorsController {
  constructor(private readonly lectorsService: LectorsControllerService) {}

  @ApiOperation({ summary: 'Create a lector' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    type: LectorResponseDto,
    description: 'Create a lector',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid fields for creation',
  })
  @HttpCode(HttpStatus.CREATED)
  @Post()
  create(@Body() createLectorDto: CreateLectorDto) {
    return this.lectorsService.create(createLectorDto);
  }

  @ApiOperation({ summary: 'Get all lectors (with sorting)' })
  @ApiQuery({
    name: 'sort',
    type: SortLectorDto,
    required: false,
    description: 'The list of lectors with ability to sort',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    isArray: true,
    type: LectorResponseDto,
    description: 'The list of all lectors',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'No students found',
  })
  @HttpCode(HttpStatus.OK)
  @Get()
  async findAllSort(@Query() sortLectorDto: SortLectorDto) {
    return await this.lectorsService.findAllSort(sortLectorDto);
  }

  @ApiOperation({ summary: 'Get all lectors' })
  @ApiResponse({
    status: HttpStatus.OK,
    type: LectorWithCoursesResponseDto,
    isArray: true,
    description: 'Get a list of all lectors',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'There are no lectors',
  })
  @HttpCode(HttpStatus.OK)
  @Get()
  findAllLectors() {
    return this.lectorsService.findAllLectors();
  }

  @UseGuards(AuthGuard)
  @ApiBearerAuth('jwt')
  @Get('me')
  public async findMe(@CurrentUser() loggedUser: any) {
    return await this.lectorsService.findCurrentLector(loggedUser.userId);
  }

  @ApiOperation({ summary: 'Get a lector by ID' })
  @ApiResponse({
    status: HttpStatus.OK,
    type: LectorWithCoursesResponseDto,
    description: 'Get a lector by ID',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'There is no lector for given ID',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid ID format',
  })
  @HttpCode(HttpStatus.OK)
  @Get(':id')
  findLectorById(@Param() lectorIdDto: LectorIdDto) {
    return this.lectorsService.findLectorById(lectorIdDto.id);
  }

  @ApiOperation({ summary: 'Remove a lector by ID' })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'Lector with given ID is removed sucessfully',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'There is no lector with given ID',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid ID format',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Delete()
  removeLector(@Param() lectorIdDto: LectorIdDto) {
    return this.lectorsService.removeLector(lectorIdDto.id);
  }

  @ApiOperation({ summary: 'Update lector by Id' })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'Lector is updated successfully',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'There is no lector with given ID',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid ID or JSON body format',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Patch(':id')
  async updateLectorById(
    @Param() lectorIdDto: LectorIdDto,
    @Body() updateLectorDto: UpdateLectorDto,
  ) {
    return this.lectorsService.updateLectorById(
      lectorIdDto.id,
      updateLectorDto,
    );
  }

  @ApiOperation({ summary: 'Add lector to couse' })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'Lector is added to the course sucessfully',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'There is no lector or course with given ID',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid ID or JSON body format',
  })
  @ApiResponse({
    status: HttpStatus.CONFLICT,
    description:
      'Lector with given ID already exist on the course with given ID',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Patch(':id')
  async addLectorToCourse(
    @Param() lectorIdDto: LectorIdDto,
    @Body() addLectorToCourseDto: AddLectorToCourseDto,
  ) {
    return this.lectorsService.addLectorToCourse(
      lectorIdDto.id,
      addLectorToCourseDto.courseId,
    );
  }
}
