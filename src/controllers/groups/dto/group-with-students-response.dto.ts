import { IsArray, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Student } from 'src/services/students/entities/student.entity';
import { GroupResponseDto } from './group-response.dto';

export class GroupWithStudentsResponseDto extends GroupResponseDto {
  @IsNotEmpty()
  @IsArray()
  @ApiProperty()
  students: Student[];
}
