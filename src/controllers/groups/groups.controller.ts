import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpStatus,
  HttpCode,
  Query,
  UseGuards,
} from '@nestjs/common';
import { GroupsControllerService } from './groups.controller.service';
import { CreateGroupDto } from './dto/create-group.dto';
import { UpdateGroupDto } from './dto/update-group.dto';
import { GroupIdDto } from './dto/group-id.dto';
import { ApiOperation, ApiQuery, ApiResponse } from '@nestjs/swagger';
import { GroupResponseDto } from './dto/group-response.dto';
import { GroupWithStudentsResponseDto } from './dto/group-with-students-response.dto';
import { SortGroupDto } from './dto/sort-group.dto';
import { AuthGuard } from 'src/services/auth/guards/auth.guard';

@UseGuards(AuthGuard)
@Controller('groups')
export class GroupsController {
  constructor(
    private readonly groupsControllerService: GroupsControllerService,
  ) {}

  @ApiOperation({ summary: 'Get all groups (with sorting)' })
  @ApiQuery({
    name: 'sort',
    type: SortGroupDto,
    required: false,
    description: 'The list of all groups with ability to sort',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    isArray: true,
    type: GroupResponseDto,
    description: 'The list of all groups',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'No groups found',
  })
  @HttpCode(HttpStatus.OK)
  @Get()
  async findAllSort(@Query() sortGroupDto: SortGroupDto) {
    return await this.groupsControllerService.findAllSort(sortGroupDto);
  }

  @ApiOperation({ summary: 'Create a group' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    type: GroupResponseDto,
    description: 'Create a group',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid fields for creation',
  })
  @HttpCode(HttpStatus.CREATED)
  @Post()
  createGroup(@Body() createGroupDto: CreateGroupDto) {
    return this.groupsControllerService.createGroup(createGroupDto);
  }

  @ApiOperation({ summary: 'Get all groups' })
  @ApiResponse({
    status: HttpStatus.OK,
    isArray: true,
    type: GroupWithStudentsResponseDto,
    description: 'The list of all groups',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'There are no groups',
  })
  @HttpCode(HttpStatus.OK)
  @Get()
  findAllGroups() {
    return this.groupsControllerService.findAllGroups();
  }

  @ApiOperation({ summary: 'Get a group by ID' })
  @ApiResponse({
    status: HttpStatus.OK,
    type: GroupWithStudentsResponseDto,
    description: 'The group by given ID',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'There is no group with given ID',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid ID format',
  })
  @HttpCode(HttpStatus.OK)
  @Get(':id')
  findGroupById(@Param() groupIdDto: GroupIdDto) {
    return this.groupsControllerService.findGroupById(groupIdDto.id);
  }

  @ApiOperation({ summary: 'Update group by ID' })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'The group was successfully updated',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'No group with given ID found',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid ID or JSON body format',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Patch(':id')
  updateGroupByID(
    @Param() groupIdDto: GroupIdDto,
    @Body() updateGroupDto: UpdateGroupDto,
  ) {
    return this.groupsControllerService.updateGroupByID(
      groupIdDto.id,
      updateGroupDto,
    );
  }

  @ApiOperation({ summary: 'Remove group by ID' })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'The group was successfully removed',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'No group with given ID found',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid group ID format',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Delete(':id')
  removeGroupById(@Param() groupIdDto: GroupIdDto) {
    return this.groupsControllerService.removeGroupById(groupIdDto.id);
  }
}
