import { Injectable } from '@nestjs/common';
import { GroupsService } from '../../services/groups/groups.service';
import { CreateGroupDto } from './dto/create-group.dto';
import { UpdateGroupDto } from './dto/update-group.dto';
import { SortGroupDto } from './dto/sort-group.dto';

@Injectable()
export class GroupsControllerService {
  constructor(private readonly groupsService: GroupsService) {}

  createGroup(createGroupDto: CreateGroupDto) {
    return this.groupsService.createGroup(createGroupDto);
  }

  findAllSort(sortStudentDto: SortGroupDto) {
    return this.groupsService.findAllSort(sortStudentDto);
  }

  async findAllGroups() {
    return await this.groupsService.findAllGroups();
  }

  async findGroupById(id: string) {
    return await this.groupsService.findGroupById(id);
  }

  async updateGroupByID(id: string, updateGroupDto: UpdateGroupDto) {
    return await this.groupsService.updateGroupByID(id, updateGroupDto);
  }

  async removeGroupById(id: string) {
    return await this.groupsService.removeGroupById(id);
  }
}
