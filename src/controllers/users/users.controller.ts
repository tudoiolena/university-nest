// import { Controller, Get, UseGuards } from '@nestjs/common';
// import { UsersControllerService } from './users.controller.service';
// import { CurrentUser } from '../../services/auth/decorators/current.user.decorator';
// import { User } from '../../services/users/users.service';
// import { AuthGuard } from '../../services/auth/guards/auth.guard';
// import { ApiBearerAuth } from '@nestjs/swagger';
// import { LectorsControllerService } from '../lectors/lectors.controller.service';
// import { Lector } from 'src/services/lectors/entities/lector.entity';

// @UseGuards(AuthGuard)
// @Controller('lectors')
// @ApiBearerAuth('jwt')
// export class UsersController {
//   constructor(
//     private readonly usersControllerService: LectorsControllerService,
//   ) {}

//   @Get('me')
//   public async findMe(@CurrentUser() loggedUser: Lector) {
//     console.log(loggedUser);
//     return await this.LectorsControllerService.findCurrentUser(loggedUser.id);
//   }
// }
