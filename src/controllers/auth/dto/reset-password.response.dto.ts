import { IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ResetPasswordResponseDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  useremail: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  token: string;
}
