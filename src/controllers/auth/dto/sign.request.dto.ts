import { IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class SignRequestDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  useremail: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  password: string;
}
