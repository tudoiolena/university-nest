import { IsDate, IsNotEmpty, IsString, IsUUID } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ResetPasswordWithTokenResponseDto {
  @IsNotEmpty()
  @IsString()
  @IsUUID()
  @ApiProperty()
  id: string;

  @IsNotEmpty()
  @IsDate()
  @ApiProperty()
  createdAt: Date;

  @IsNotEmpty()
  @IsDate()
  @ApiProperty()
  updatedAt: Date;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  token: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  lector_email: string;
}
