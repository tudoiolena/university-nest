import { Module } from '@nestjs/common';
import { MarksController } from './marks.controller';
import { MarksControllerService } from './marks.controller.service';
import { MarksStudentsCoursesLectorsModule } from '../../services/marks/marks-students-courses-lectors.module';
import { MarksModule } from 'src/services/marks/marks.module';

@Module({
  imports: [MarksStudentsCoursesLectorsModule, MarksModule],
  controllers: [MarksController],
  providers: [MarksControllerService],
})
export class MarksControllerModule {}
