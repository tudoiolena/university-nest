import { Injectable } from '@nestjs/common';
import { MarksStudentsCoursesLectorsService } from '../../services/marks/marks-students-courses-lectors.service';
import { AddMarkToStudentDto } from './dto/add-mark-to-student.dto';

import { MarksService } from '../../services/marks/marks.service';

@Injectable()
export class MarksControllerService {
  constructor(
    private readonly marksStudentsCoursesLectorsService: MarksStudentsCoursesLectorsService,
    private readonly marksService: MarksService,
  ) {}

  getAllMarks() {
    return this.marksService.getAllMarks();
  }

  getMarksByStudentId(studentId: string) {
    return this.marksStudentsCoursesLectorsService.getMarksByStudentId(
      studentId,
    );
  }

  getMarksByCourseId(courseId: string) {
    return this.marksStudentsCoursesLectorsService.getMarksByCourseId(courseId);
  }

  addMarkToStudent(
    studentId: string,
    addMarkToStudentDto: AddMarkToStudentDto,
  ) {
    return this.marksStudentsCoursesLectorsService.addMarkToStudent(
      studentId,
      addMarkToStudentDto,
    );
  }
}
