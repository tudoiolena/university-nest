import { IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class MarksByCourseIdResponseDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  courseName: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  lectorName: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  studentName: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  mark: string;
}
