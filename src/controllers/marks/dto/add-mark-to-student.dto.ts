import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsUUID } from 'class-validator';

export class AddMarkToStudentDto {
  @IsUUID()
  @ApiProperty()
  @IsNotEmpty()
  courseId: string;

  @IsUUID()
  @ApiProperty()
  @IsNotEmpty()
  lectorId: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  mark: string;
}
