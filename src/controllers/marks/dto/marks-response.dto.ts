import { IsDate, IsNotEmpty, IsString, IsUUID } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class MarksResponseDto {
  @IsNotEmpty()
  @IsString()
  @IsUUID()
  @ApiProperty()
  markId: string;

  @IsNotEmpty()
  @IsDate()
  @ApiProperty()
  markCreatedAt: Date;

  @IsNotEmpty()
  @IsDate()
  @ApiProperty()
  markUpdatedAt: Date;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  courseName: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  lectorName: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  studentName: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  mark: string;
}
