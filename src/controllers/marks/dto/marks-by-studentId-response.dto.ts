import { IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class MarksByStudentIdResponseDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  courseName: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  mark: string;
}
