import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsUUID } from 'class-validator';

export class StudentCourseIdDto {
  @IsOptional()
  @IsUUID()
  @IsOptional()
  @ApiPropertyOptional()
  student_id: string;

  @IsOptional()
  @IsUUID()
  @IsOptional()
  @ApiPropertyOptional()
  course_id: string;
}
