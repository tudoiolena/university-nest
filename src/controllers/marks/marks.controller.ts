import {
  Controller,
  Get,
  Body,
  Patch,
  Param,
  Query,
  HttpStatus,
  HttpCode,
} from '@nestjs/common';
import { MarksControllerService } from './marks.controller.service';
import { AddMarkToStudentDto } from './dto/add-mark-to-student.dto';
import { StudentCourseIdDto } from './dto/student-course-id.dto';
import { ApiOperation, ApiQuery, ApiResponse } from '@nestjs/swagger';
import { MarksResponseDto } from './dto/marks-response.dto';
import { MarksByStudentIdResponseDto } from './dto/marks-by-studentId-response.dto';
import { MarksByCourseIdResponseDto } from './dto/marks-by-courseId-response.dto';
import { StudentIdDto } from '../students/dto/student-id.dto';

@Controller('marks')
export class MarksController {
  constructor(private readonly marksService: MarksControllerService) {}

  @ApiOperation({ summary: 'Get all marks' })
  @ApiQuery({
    name: 'studentId',
    type: StudentCourseIdDto,
    required: false,
    description: 'ID of the student to filter marks by student ID',
  })
  @ApiQuery({
    name: 'courseId',
    type: StudentCourseIdDto,
    required: false,
    description: 'ID of the course to filter marks by course',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    isArray: true,
    type: MarksByStudentIdResponseDto,
    description: 'The list of all marks filtered by student ID',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    isArray: true,
    type: MarksByCourseIdResponseDto,
    description: 'The list of all marks filtered by course ID',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    isArray: true,
    type: MarksResponseDto,
    description: 'The list of all marks',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'No marks found for the student or course with given ID',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid student or course ID format',
  })
  @HttpCode(HttpStatus.OK)
  @Get()
  findAll(@Query() studentCourseIdDto: StudentCourseIdDto) {
    if (studentCourseIdDto.student_id) {
      return this.marksService.getMarksByStudentId(
        studentCourseIdDto.student_id,
      );
    } else if (studentCourseIdDto.course_id) {
      return this.marksService.getMarksByCourseId(studentCourseIdDto.course_id);
    } else {
      return this.marksService.getAllMarks();
    }
  }

  @ApiOperation({ summary: 'Add a mark to a student' })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'The mark was successfully added to the student',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'No student or course or lector with given ID found',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid ID or JSON body format',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Patch(':id')
  async addMarkToStudent(
    @Param() studentIdDto: StudentIdDto,
    @Body() addMarkToStudentDto: AddMarkToStudentDto,
  ) {
    return await this.marksService.addMarkToStudent(
      studentIdDto.id,
      addMarkToStudentDto,
    );
  }
}
