import { IsNumber, IsString } from 'class-validator';

export class ImageResponseDto {
  @IsString()
  public fieldname: string;

  @IsString()
  public originalname: string;

  @IsString()
  public encoding: string;

  @IsString()
  public mimetype: string;

  @IsString()
  public destination: string;

  @IsString()
  public filename: string;

  @IsString()
  public path: string;

  @IsNumber()
  public size: number;
}
