import { IsNumber, IsOptional, IsString } from 'class-validator';
import { ApiPropertyOptional } from '@nestjs/swagger';

export class SortStudentDto {
  @IsString()
  @IsOptional()
  @ApiPropertyOptional()
  public sortField?: string;

  @IsString()
  @IsOptional()
  @ApiPropertyOptional()
  public sortOrder?: string = 'DESC';

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  public name?: string;

  @IsOptional()
  @IsNumber()
  @ApiPropertyOptional()
  limit: number;

  @IsOptional()
  @IsNumber()
  @ApiPropertyOptional()
  offset: number;
}
