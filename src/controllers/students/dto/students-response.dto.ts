import {
  IsDate,
  IsEmail,
  IsNotEmpty,
  IsNumber,
  IsString,
  IsUUID,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class StudentsResponseDto {
  @IsNotEmpty()
  @IsString()
  @IsUUID()
  @ApiProperty()
  id: string;

  @IsNotEmpty()
  @IsDate()
  @ApiProperty()
  createdAt: Date;

  @IsNotEmpty()
  @IsDate()
  @ApiProperty()
  updatedAt: Date;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  name: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  surname: string;

  @IsNotEmpty()
  @IsEmail()
  @ApiProperty()
  email: string;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  age: number;
}
