import { IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { ApiPropertyOptional } from '@nestjs/swagger';

export class StudentNameDto {
  @IsNotEmpty()
  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  name: string;
}
