import { IsNotEmpty, IsString, IsUUID } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class AddStudentToCourseDto {
  @IsNotEmpty()
  @IsString()
  @IsUUID()
  @ApiProperty()
  id: string;
}
