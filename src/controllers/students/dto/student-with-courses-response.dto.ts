import { IsArray, IsNotEmpty, IsOptional } from 'class-validator';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { Course } from 'src/services/courses/entities/course.entity';
import { StudentsResponseDto } from './students-response.dto';

export class StudentWithCoursesResponseDto extends StudentsResponseDto {
  @IsNotEmpty()
  @IsArray()
  @ApiPropertyOptional()
  @IsOptional()
  courses?: Course[];
}
