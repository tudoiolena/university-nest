import { Module } from '@nestjs/common';
import { StudentsControllerService } from './students.controller.service';
import { StudentsController } from './students.controller';
import { StudentsModule } from '../../services/students/students.module';
import { StudentsCoursesModule } from 'src/services/students/students-courses.module';

@Module({
  imports: [StudentsModule, StudentsCoursesModule],
  controllers: [StudentsController],
  providers: [StudentsControllerService],
})
export class StudentsControllerModule {}
