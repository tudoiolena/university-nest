import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  HttpStatus,
  HttpCode,
  UseInterceptors,
  UploadedFile,
  Res,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { StudentsControllerService } from './students.controller.service';
import { CreateStudentDto } from './dto/create-student.dto';
import { UpdateStudentDto } from './dto/update-student.dto';
import { ApiOperation, ApiQuery, ApiResponse } from '@nestjs/swagger';
import { StudentsResponseDto } from './dto/students-response.dto';
import { StudentIdDto } from './dto/student-id.dto';
import { StudentsWithGroupNameResponseDto } from './dto/students-with-group-name-response.dto';
import { SortStudentDto } from './dto/sort-student.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { AddStudentToCourseDto } from './dto/add-student-to-course.dto';
import { AuthGuard } from 'src/services/auth/guards/auth.guard';

// export const storage = {
//   storage: diskStorage({
//     destination: './uploads/profileimages',
//     filename: (req, file, cb) => {
//       const filename: string =
//         path.parse(file.originalname).name.replace(/\s/g, ' ') + uuidv4();
//       const extension: string = path.parse(file.originalname).ext;

//       cb(null, `${filename}${extension}`);
//     },
//   }),
// };
@Controller('students')
export class StudentsController {
  constructor(
    private readonly studentsControllerService: StudentsControllerService,
  ) {}

  @ApiOperation({ summary: 'Get all students (with sorting)' })
  @ApiQuery({
    name: 'sort',
    type: SortStudentDto,
    required: false,
    description: 'The list of students with ability to sort',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    isArray: true,
    type: StudentsResponseDto,
    description: 'The list of all students',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'No students found',
  })
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.OK)
  @Get()
  async findAllStudents(@Query() sortStudentDto: SortStudentDto) {
    return await this.studentsControllerService.findAllStudents(sortStudentDto);
  }

  @ApiOperation({ summary: 'Create a student' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    type: StudentsResponseDto,
    description: 'Create a student',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid fields for creation',
  })
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AuthGuard)
  @Post()
  createStudent(@Body() createStudentDto: CreateStudentDto) {
    return this.studentsControllerService.createStudent(createStudentDto);
  }

  @ApiOperation({ summary: 'Get a student by given ID' })
  @ApiResponse({
    status: HttpStatus.OK,
    isArray: true,
    type: StudentsWithGroupNameResponseDto,
    description: 'The students by Id',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'No student found',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid id format',
  })
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @Get(':id')
  findStudentById(@Param() studentIdValidationDto: StudentIdDto) {
    return this.studentsControllerService.findStudentById(
      studentIdValidationDto.id,
    );
  }

  @ApiOperation({ summary: 'Update a student by given ID' })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'Student updated sucsessfully',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'No student with given ID found',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid ID format',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @UseGuards(AuthGuard)
  @Patch(':id')
  updateStudentById(
    @Param() studentIdValidationDto: StudentIdDto,
    @Body() updateStudentDto: UpdateStudentDto,
  ) {
    if (updateStudentDto.course_id) {
      return this.studentsControllerService.addStudentToCourse(
        studentIdValidationDto.id,
        updateStudentDto.course_id,
      );
    } else {
      return this.studentsControllerService.updateStudentById(
        studentIdValidationDto,
        updateStudentDto,
      );
    }
  }

  @ApiOperation({ summary: 'Delete a student by given ID' })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'Student removed sucsessfully',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'No student with given ID found',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid ID format',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @UseGuards(AuthGuard)
  @Delete(':id')
  removeStudentById(@Param() studentIdValidationDto: StudentIdDto) {
    return this.studentsControllerService.removeStudentById(
      studentIdValidationDto,
    );
  }

  // @Post('upload/:id')
  // @UseInterceptors(FileInterceptor('file', storage))
  // async uploadFile(@UploadedFile() file, @Param() studentIdDto: StudentIdDto) {
  //   return this.studentsControllerService
  //     .updateOne(studentIdDto.id, {
  //       imagePath: file.filename,
  //     })
  //     .pipe(map((student) => ({ imagePath: student.imagePath })));
  // }

  // @Get('image/:imagename')
  // findProfileImage(@Param('imagename') imagename, @Res() res) {
  //   return of(
  //     res.sendFile(join(process.cwd(), 'uploads/profileimages/' + imagename)),
  //   );
  // }
  @UseGuards(AuthGuard)
  @Post('upload/:id')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads',
        filename: (req, file, cb) => {
          const name = file.originalname.split('.')[0];
          const fileExtension = file.originalname.split('.')[1];
          const newFilename =
            name.split(' ').join('_') + '_' + Date.now() + '.' + fileExtension;

          cb(null, newFilename);
        },
      }),
      fileFilter: (req, file, cb) => {
        if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
          return cb(null, false);
        }
        cb(null, true);
      },
    }),
  )
  uploadPhoto(
    @Param() studentIdValidationDto: StudentIdDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (!file) {
      throw new BadRequestException('File is not an image');
    } else {
      // const port = process.env.APP_PORT;
      const updateStudentDto = new UpdateStudentDto();
      updateStudentDto.imagePath = `https://university-nest.onrender.com/api/v1/students/images/${file.filename}`;

      return this.studentsControllerService.updateStudentById(
        studentIdValidationDto,
        updateStudentDto,
      );
    }
  }

  @Get('images/:imagename')
  async getProfileImage(@Param('imagename') imagename, @Res() res) {
    res.sendFile(imagename, { root: './uploads' });
  }

  @ApiOperation({ summary: 'Add student to couse' })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'Student is added to the course sucessfully',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'There is no student or course with given ID',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid ID or JSON body format',
  })
  @ApiResponse({
    status: HttpStatus.CONFLICT,
    description:
      'Student with given ID already exist on the course with given ID',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @UseGuards(AuthGuard)
  @Patch(':id')
  async addStudentToCourse(
    @Param() studentIdDto: StudentIdDto,
    @Body() addStudentToCourseDto: AddStudentToCourseDto,
  ) {
    return this.studentsControllerService.addStudentToCourse(
      studentIdDto.id,
      addStudentToCourseDto.id,
    );
  }
}
