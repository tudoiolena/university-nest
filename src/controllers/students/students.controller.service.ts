import { Injectable } from '@nestjs/common';
import { StudentsService } from '../../services/students/students.service';
import { CreateStudentDto } from './dto/create-student.dto';
import { UpdateStudentDto } from './dto/update-student.dto';
import { StudentIdDto } from './dto/student-id.dto';
import { SortStudentDto } from './dto/sort-student.dto';
import { StudentsCoursesService } from 'src/services/students/students-courses.service';

@Injectable()
export class StudentsControllerService {
  constructor(
    private readonly studentsService: StudentsService,
    private readonly studentCoursesService: StudentsCoursesService,
  ) {}

  createStudent(createStudentDto: CreateStudentDto) {
    return this.studentsService.createStudent(createStudentDto);
  }

  findAllStudents(sortStudentDto: SortStudentDto) {
    return this.studentsService.findAllStudents(sortStudentDto);
  }

  findStudentById(id: string) {
    return this.studentsService.findStudentById(id);
  }

  updateStudentById(
    studentIdValidationDto: StudentIdDto,
    updateStudentDto: UpdateStudentDto,
  ) {
    return this.studentsService.updateStudent(
      studentIdValidationDto,
      updateStudentDto,
    );
  }

  updateOne(id: string, student) {
    return this.studentsService.updateOne(id, student);
  }

  removeStudentById(studentIdValidationDto: StudentIdDto) {
    return this.studentsService.removeStudent(studentIdValidationDto);
  }

  addStudentToCourse(studentId: string, courseId: string) {
    return this.studentCoursesService.addStudentToCourse(studentId, courseId);
  }
}
