import { Injectable } from '@nestjs/common';
import { CoursesService } from '../../services/courses/courses.service';
import { CreateCourseDto } from './dto/create-course.dto';
import { UpdateCourseDto } from './dto/update-course.dto';
import { SortCourseDto } from './dto/sort-course.dto';

@Injectable()
export class CoursesControllerService {
  constructor(private readonly coursesService: CoursesService) {}

  createCourse(createCourseDto: CreateCourseDto) {
    return this.coursesService.createCourse(createCourseDto);
  }

  findAllCourses() {
    return this.coursesService.findAllCourses();
  }

  findAllSort(sortCourseDto: SortCourseDto) {
    return this.coursesService.findAllSort(sortCourseDto);
  }

  findCourseById(id: string) {
    return this.coursesService.findCourseById(id);
  }

  updateCourse(id: string, updateCourseDto: UpdateCourseDto) {
    return this.coursesService.updateCourse(id, updateCourseDto);
  }

  removeCourse(id: string) {
    return this.coursesService.removeCourse(id);
  }

  getAllCoursesByLectorId(lectorId: string) {
    return this.coursesService.getAllCoursesByLectorId(lectorId);
  }
}
