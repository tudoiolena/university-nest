import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Query,
  HttpStatus,
  HttpCode,
} from '@nestjs/common';
import { CreateCourseDto } from './dto/create-course.dto';
import { UpdateCourseDto } from './dto/update-course.dto';
import { CoursesControllerService } from './courses.controller.service';
import { AuthGuard } from '../../services/auth/guards/auth.guard';
import { LectorIdDto } from './dto/lector-id.dto';
import { ApiOperation, ApiQuery, ApiResponse } from '@nestjs/swagger';
import { CourseResponseDto } from './dto/course-response.dto';
import { CourseResponseByLectorIdDto } from './dto/course-response-by-lector-id.dto';
import { CourseIdDto } from './dto/course-id.dto';
import { SortCourseDto } from './dto/sort-course.dto';

@UseGuards(AuthGuard)
@Controller('courses')
export class CoursesController {
  constructor(
    private readonly coursesControllerService: CoursesControllerService,
  ) {}

  @ApiOperation({ summary: 'Create a course' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    type: CourseResponseDto,
    description: 'Create a course',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid fields for creation',
  })
  @HttpCode(HttpStatus.CREATED)
  @Post()
  createCourse(@Body() createCourseDto: CreateCourseDto) {
    return this.coursesControllerService.createCourse(createCourseDto);
  }

  @ApiOperation({ summary: 'Get all courses (with sorting)' })
  @ApiQuery({
    name: 'sort',
    type: SortCourseDto,
    required: false,
    description: 'The list of all courses with ability to sort',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    isArray: true,
    type: CourseResponseDto,
    description: 'The list of all courses',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'No courses found',
  })
  @HttpCode(HttpStatus.OK)
  @Get()
  async findAllSort(@Query() sortCourseDto: SortCourseDto) {
    return await this.coursesControllerService.findAllSort(sortCourseDto);
  }

  @ApiOperation({ summary: 'Get all courses' })
  @ApiResponse({
    status: HttpStatus.OK,
    type: CourseResponseDto,
    isArray: true,
    description: 'The list of all courses',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    type: CourseResponseByLectorIdDto,
    description: 'The course by given ID',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'There are no courses for the lector with given ID',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid lector ID format',
  })
  @HttpCode(HttpStatus.OK)
  @Get()
  findAllCourses(@Query() lectorIdDto: LectorIdDto) {
    if (lectorIdDto.lector_id) {
      return this.coursesControllerService.getAllCoursesByLectorId(
        lectorIdDto.lector_id,
      );
    } else {
      return this.coursesControllerService.findAllCourses();
    }
  }

  @ApiOperation({ summary: 'Get a course by ID' })
  @ApiResponse({
    status: HttpStatus.OK,
    type: CourseResponseDto,
    description: 'The course by given ID',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'There are no courses for given ID',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid course ID format',
  })
  @HttpCode(HttpStatus.OK)
  @Get(':id')
  findCourseById(@Param() courseIdDto: CourseIdDto) {
    return this.coursesControllerService.findCourseById(courseIdDto.id);
  }

  @ApiOperation({ summary: 'Update a course by ID' })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'The corse with given ID is updated sucessfully',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'There are no course with given ID',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid course ID format',
  })
  @HttpCode(HttpStatus.OK)
  @Patch(':id')
  async updateCourse(
    @Param() courseIdDto: CourseIdDto,
    @Body() updateCourseDto: UpdateCourseDto,
  ) {
    return this.coursesControllerService.updateCourse(
      courseIdDto.id,
      updateCourseDto,
    );
  }

  @ApiOperation({ summary: 'Remove a course by ID' })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'The corse with given ID is removed sucessfully',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'There are no course with given ID',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid course ID format',
  })
  @HttpCode(HttpStatus.OK)
  @Delete(':id')
  removeCourse(@Param() courseIdDto: CourseIdDto) {
    return this.coursesControllerService.removeCourse(courseIdDto.id);
  }
}
