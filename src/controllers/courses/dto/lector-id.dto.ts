import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsUUID } from 'class-validator';

export class LectorIdDto {
  @IsOptional()
  @IsUUID()
  @ApiProperty()
  @ApiPropertyOptional()
  lector_id: string;
}
