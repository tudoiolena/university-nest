import { IsOptional, IsString } from 'class-validator';
import { ApiPropertyOptional } from '@nestjs/swagger';

export class SortCourseDto {
  @IsString()
  @IsOptional()
  @ApiPropertyOptional()
  public sortField?: string;

  @IsString()
  @IsOptional()
  @ApiPropertyOptional()
  public sortOrder?: string = 'DESC';

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  public name?: string;
}
