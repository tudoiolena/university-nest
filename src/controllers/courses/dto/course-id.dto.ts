import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString, IsUUID } from 'class-validator';

export class CourseIdDto {
  @IsString()
  @IsUUID()
  @IsOptional()
  @ApiPropertyOptional()
  id: string;
}
