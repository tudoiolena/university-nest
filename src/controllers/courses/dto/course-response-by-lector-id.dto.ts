import { IsNotEmpty, IsNumber, IsString, IsUUID } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CourseResponseByLectorIdDto {
  @IsNotEmpty()
  @IsString()
  @IsUUID()
  @ApiProperty()
  courseId;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  courseName: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  courseDesciption: string;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  courseHours: number;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  lectorName;
}
