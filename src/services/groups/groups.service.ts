import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateGroupDto } from '../../controllers/groups/dto/create-group.dto';
import { UpdateGroupDto } from '../../controllers/groups/dto/update-group.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { Group } from './entities/group.entity';
import { GroupResponseDto } from '../../controllers/groups/dto/group-response.dto';
import { GroupWithStudentsResponseDto } from '../../controllers/groups/dto/group-with-students-response.dto';
import { SortGroupDto } from '../../controllers/groups/dto/sort-group.dto';

@Injectable()
export class GroupsService {
  constructor(
    @InjectRepository(Group)
    private readonly groupRepository: Repository<Group>,
  ) {}

  createGroup(createGroupDto: CreateGroupDto): Promise<GroupResponseDto> {
    return this.groupRepository.save(createGroupDto);
  }

  async findAllSort(sortGroupDto: SortGroupDto): Promise<Group[]> {
    let groups = [];

    if (sortGroupDto.name) {
      groups = await this.groupRepository.find({
        where: { name: sortGroupDto.name },
      });
    } else if (sortGroupDto.sortOrder) {
      sortGroupDto.sortField = 'name';
      groups = await this.groupRepository.find({
        order: {
          [sortGroupDto.sortField]: sortGroupDto.sortOrder,
        },
      });
    } else {
      groups = await this.groupRepository.find();
    }

    if (groups.length === 0) {
      throw new NotFoundException('There no groups yet');
    }
    return groups;
  }

  async findAllGroups(): Promise<GroupWithStudentsResponseDto[]> {
    const groups = await this.groupRepository.find({ relations: ['students'] });
    if (groups.length === 0) {
      throw new NotFoundException('There no groups yet');
    }
    return groups;
  }

  async findGroupById(id: string): Promise<GroupWithStudentsResponseDto> {
    const group = await this.groupRepository
      .createQueryBuilder('group')
      .leftJoinAndSelect('group.students', 'student')
      .where('group.id = :id', { id })
      .getOne();
    if (!group) {
      throw new NotFoundException(`Group with #${id} ID not found`);
    }
    return group;
  }

  async updateGroupByID(
    id: string,
    updateGroupDto: UpdateGroupDto,
  ): Promise<UpdateResult> {
    const group = await this.groupRepository.findOneBy({
      id,
    });

    if (!group) {
      throw new NotFoundException(`Group with #${id} ID not found`);
    }
    return await this.groupRepository.update(id, updateGroupDto);
  }

  async removeGroupById(id: string): Promise<DeleteResult> {
    const group = await this.groupRepository.findOneBy({
      id,
    });

    if (!group) {
      throw new NotFoundException(`Group with #${id} ID not found`);
    }
    return await this.groupRepository.delete(id);
  }
}
