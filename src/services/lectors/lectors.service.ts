import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateLectorDto } from '../../controllers/lectors/dto/create-lector.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { Lector } from './entities/lector.entity';
import { LectorResponseDto } from '../../controllers/lectors/dto/lector-response.dto';
import { LectorWithCoursesResponseDto } from '../../controllers/lectors/dto/lector-with-courses-response.dto';
import { UpdateLectorDto } from '../../controllers/lectors/dto/update-lector.dto';
import * as bcrypt from 'bcrypt';
import { SortLectorDto } from '../../controllers/lectors/dto/sort-lector.dto';

@Injectable()
export class LectorsService {
  constructor(
    @InjectRepository(Lector)
    private readonly lectorRepository: Repository<Lector>,
  ) {}

  async createLector(
    createLectorDto: CreateLectorDto,
  ): Promise<LectorResponseDto> {
    const { password, ...rest } = createLectorDto;

    const hashedPassword = await bcrypt.hash(password, 10);

    const lector = this.lectorRepository.create({
      ...rest,
      password: hashedPassword,
    });

    return await this.lectorRepository.save(lector);
  }

  async findAllSort(sortLectorDto: SortLectorDto): Promise<Lector[]> {
    let lectors = [];

    if (sortLectorDto.name) {
      lectors = await this.lectorRepository.find({
        where: { name: sortLectorDto.name },
      });
    } else if (sortLectorDto.sortOrder) {
      sortLectorDto.sortField = 'name';
      lectors = await this.lectorRepository.find({
        order: {
          [sortLectorDto.sortField]: sortLectorDto.sortOrder,
        },
      });
    } else {
      lectors = await this.lectorRepository.find();
    }

    if (lectors.length === 0) {
      throw new NotFoundException('There no lectors yet');
    }
    return lectors;
  }

  public async findAllLectors(): Promise<LectorWithCoursesResponseDto[]> {
    const lectors = await this.lectorRepository.find({
      relations: ['courses'],
    });
    if (lectors.length === 0) {
      throw new NotFoundException('There no lectors yet');
    }
    return lectors;
  }

  public async findLectorById(
    id: string,
  ): Promise<LectorWithCoursesResponseDto> {
    const lector = await this.lectorRepository.findOne({
      where: { id },
      relations: ['courses'],
    });

    if (!lector) {
      throw new NotFoundException(`Lector with #${id} ID not found`);
    }
    return lector;
  }

  public async removeLector(id: string): Promise<DeleteResult> {
    const lector = await this.lectorRepository.findOneBy({
      id,
    });

    if (!lector) {
      throw new NotFoundException(`Lector with #${id} ID not found`);
    }
    return await this.lectorRepository.delete(id);
  }

  public async updateLector(id: string, updateLecrorDto: UpdateLectorDto) {
    const lector = await this.lectorRepository.findOneBy({
      id,
    });

    if (!lector) {
      throw new NotFoundException(`Lector with #${id} ID not found`);
    }

    return await this.lectorRepository.update(id, updateLecrorDto);
  }

  public async findOneByEmail(email: string): Promise<LectorResponseDto> {
    return await this.lectorRepository.findOneBy({ email });
  }

  public async findById(id: string): Promise<LectorResponseDto> {
    return await this.lectorRepository.findOneBy({ id });
  }
}
