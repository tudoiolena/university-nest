import { Module } from '@nestjs/common';

import { LectorsCoursesService } from './lectors-courses.service';
import { LectorsModule } from './lectors.module';
import { CoursesModule } from '../courses/courses.module';

@Module({
  imports: [LectorsModule, CoursesModule],
  exports: [LectorsCoursesService],
  providers: [LectorsCoursesService],
})
export class LectorsCoursesModule {}
