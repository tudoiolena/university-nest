import { ConflictException, Injectable } from '@nestjs/common';
import { LectorsService } from './lectors.service';
import { CoursesService } from '../courses/courses.service';
import { LectorResponseDto } from '../../controllers/lectors/dto/lector-response.dto';
import { Lector } from './entities/lector.entity';
import { Course } from '../courses/entities/course.entity';

@Injectable()
export class LectorsCoursesService {
  constructor(
    private readonly lectorService: LectorsService,
    private readonly courseService: CoursesService,
  ) {}

  async addLectorToCourse(
    lectorId: string,
    courseId: string,
  ): Promise<LectorResponseDto> {
    const lector = await this.lectorService.findLectorById(lectorId);

    const course = await this.courseService.findCourseWithLectors(courseId);

    if (
      lector.courses &&
      lector.courses.some((existingCourse) => existingCourse.id === course.id)
    ) {
      throw new ConflictException('Lector already exists on this course');
    }

    if (!lector.courses) {
      lector.courses = [];
    }

    if (!course.lectors) {
      course.lectors = [];
    }

    lector.courses.push(course as Course);
    course.lectors.push(lector as Lector);

    await this.lectorService.createLector(lector);
    await this.courseService.createCourse(course);

    return lector;
  }
}
