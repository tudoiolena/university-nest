import { Module } from '@nestjs/common';
import { LectorsService } from './lectors.service';
import { Lector } from './entities/lector.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Lector])],
  exports: [LectorsService],
  providers: [LectorsService],
})
export class LectorsModule {}
