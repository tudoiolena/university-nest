import { Course } from '../../courses/entities/course.entity';
import { CoreEntity } from '../../../application/entities/core.entity';
import { Column, Entity, JoinTable, ManyToMany, OneToMany } from 'typeorm';
import { Mark } from '../../marks/entities/mark.entity';
import { Exclude } from 'class-transformer';

@Entity({ name: 'lectors' })
export class Lector extends CoreEntity {
  @Column({
    type: 'varchar',
    nullable: true,
  })
  name: string;

  @Column({
    type: 'varchar',
    nullable: false,
    unique: true,
  })
  email: string;

  @Exclude()
  @Column({
    type: 'varchar',
    nullable: false,
  })
  password: string;

  @OneToMany(() => Mark, (mark) => mark.course, { cascade: ['remove'] })
  marks: Mark[];

  @ManyToMany(() => Course, (course) => course.lectors, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  @JoinTable({
    name: 'lector_course',
    joinColumn: {
      name: 'lector_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'course_id',
      referencedColumnName: 'id',
    },
  })
  courses?: Course[];
}
