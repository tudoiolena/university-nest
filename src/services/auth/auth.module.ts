import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';

import { AuthService } from './auth.service';
import { jwtConstants } from '../../application/constants/auth.constansts';
import { AuthGuard } from './guards/auth.guard';
import { ResetTokenModule } from '../reset-token/reset-token.module';
import { LectorsModule } from '../lectors/lectors.module';

@Module({
  imports: [
    LectorsModule,
    JwtModule.register({
      global: true,
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '1h' },
    }),
    ResetTokenModule,
  ],
  providers: [AuthService, AuthGuard],
  exports: [AuthService],
})
export class AuthModule {}
