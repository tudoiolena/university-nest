import {
  BadRequestException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ResetTokenService } from '../reset-token/reset-token.service';
import { SignResponseDto } from '../../controllers/auth/dto/sign.response.dto';
import { ResetPasswordWithTokenRequestDto } from '../../controllers/auth/dto/reset-password-with-token.request.dto';
import { LectorsService } from '../lectors/lectors.service';
import { MailerService } from '@nestjs-modules/mailer';
import * as bcrypt from 'bcrypt';
import { ResetPasswordWithTokenResponseDto } from '../../controllers/auth/dto/reset-password-with-token.response.dto';

@Injectable()
export class AuthService {
  constructor(
    private lectorsService: LectorsService,
    private jwtService: JwtService,
    private resetTokenService: ResetTokenService,
    private mailerService: MailerService,
  ) {}

  public async signUp(
    useremail: string,
    pass: string,
  ): Promise<SignResponseDto> {
    const lector = await this.lectorsService.createLector({
      email: useremail,
      password: pass,
    });
    const payload = { sub: lector.id, useremail: lector.email };
    return {
      accessToken: await this.jwtService.signAsync(payload),
    };
  }

  public async resetPasswordRequest(
    useremail: string,
  ): Promise<ResetPasswordWithTokenResponseDto> {
    const lector = await this.lectorsService.findOneByEmail(useremail);
    if (!lector) {
      throw new BadRequestException(
        `Cannot generate token for reset password request  because user ${useremail} is not found`,
      );
    }

    const resetToken = await this.resetTokenService.generateResetToken(
      lector.email,
    );

    const tokenObject = {
      token: resetToken.token,
      lector_email: lector.email,
    };

    const appPort = process.env.FRONT_END_PORT || 3000;

    const tokenJson = JSON.stringify(tokenObject);
    const tokenParam = encodeURIComponent(tokenJson);
    const resetLink = `http://localhost:${appPort}/reset-password?token=${tokenParam}`;

    this.sendPasswordResetEmail(useremail, resetLink);

    return resetToken;
  }

  public async resetPassword(
    resetPasswordWithTokenRequestDto: ResetPasswordWithTokenRequestDto,
  ): Promise<void> {
    const { token, useremail, newPassword } = resetPasswordWithTokenRequestDto;

    const resetPasswordRequest =
      await this.resetTokenService.getResetToken(token);

    if (!resetPasswordRequest) {
      throw new BadRequestException(
        `There is no request password request for user: ${useremail}`,
      );
    }

    const lector = await this.lectorsService.findOneByEmail(useremail);
    if (!lector) {
      throw new BadRequestException(`Lector with ${useremail} is not found`);
    }

    const hashedPassword = await bcrypt.hash(newPassword, 10);

    await this.lectorsService.updateLector(lector.id, {
      password: hashedPassword,
    });
    await this.resetTokenService.removeResetToken(token);
  }

  public async signIn(
    useremail: string,
    pass: string,
  ): Promise<SignResponseDto> {
    const lector = await this.lectorsService.findOneByEmail(useremail);
    if (!lector) {
      throw new UnauthorizedException();
    }

    const isPasswordValid = await bcrypt.compare(pass, lector.password);

    if (!isPasswordValid) {
      throw new UnauthorizedException();
    }

    const payload = { userId: lector.id, useremail: lector.email };
    return {
      accessToken: await this.jwtService.signAsync(payload),
    };
  }

  private sendPasswordResetEmail(useremail: string, resetLink: string): void {
    this.mailerService.sendMail({
      to: useremail,
      from: 'nestproject@mailinator.com',
      subject: 'Password Reset',
      text: `Click the link to reset your password: ${resetLink}`,
    });
  }
}
