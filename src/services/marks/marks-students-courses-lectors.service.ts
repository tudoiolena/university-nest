import { Injectable } from '@nestjs/common';
import { Mark } from './entities/mark.entity';
import { AddMarkToStudentDto } from '../../controllers/marks/dto/add-mark-to-student.dto';
import { MarksService } from './marks.service';
import { StudentsService } from '../students/students.service';
import { CoursesService } from '../courses/courses.service';
import { LectorsService } from '../lectors/lectors.service';
import { MarksByStudentIdResponseDto } from '../../controllers/marks/dto/marks-by-studentId-response.dto';
import { MarksByCourseIdResponseDto } from '../../controllers/marks/dto/marks-by-courseId-response.dto';

@Injectable()
export class MarksStudentsCoursesLectorsService {
  constructor(
    private readonly marksService: MarksService,
    private readonly studentsService: StudentsService,
    private readonly coursesService: CoursesService,
    private readonly lectorsService: LectorsService,
  ) {}

  async getMarksByStudentId(
    studentId: string,
  ): Promise<MarksByStudentIdResponseDto[]> {
    const student = await this.studentsService.findStudentById(studentId);
    if (student) {
      return await this.marksService.getMarksByStudentId(studentId);
    }
  }

  async getMarksByCourseId(
    courseId: string,
  ): Promise<MarksByCourseIdResponseDto[]> {
    const course = await this.coursesService.findCourseById(courseId);
    if (course) {
      return await this.marksService.getMarksByCourseId(courseId);
    }
  }

  async addMarkToStudent(
    studentId: string,
    addMarkToStudentDto: AddMarkToStudentDto,
  ): Promise<Mark> {
    const student = await this.studentsService.findStudentById(studentId);

    const course = await this.coursesService.findCourseById(
      addMarkToStudentDto.courseId,
    );

    const lector = await this.lectorsService.findLectorById(
      addMarkToStudentDto.lectorId,
    );

    const isExistingMark = await this.marksService.checkExistingMark(
      studentId,
      addMarkToStudentDto,
    );

    if (isExistingMark === null) {
      const mark = await this.marksService.addMarkToStudent(
        student,
        course,
        lector,
        addMarkToStudentDto.mark,
      );
      return mark;
    }
  }
}
