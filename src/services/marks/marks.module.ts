import { Module } from '@nestjs/common';
import { MarksService } from './marks.service';
import { Mark } from './entities/mark.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Mark])],
  exports: [MarksService],
  providers: [MarksService],
})
export class MarksModule {}
