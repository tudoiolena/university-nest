import { Module } from '@nestjs/common';
import { MarksStudentsCoursesLectorsService } from './marks-students-courses-lectors.service';
import { StudentsModule } from '../students/students.module';
import { CoursesModule } from '../courses/courses.module';
import { LectorsModule } from '../lectors/lectors.module';
import { MarksModule } from './marks.module';

@Module({
  imports: [StudentsModule, CoursesModule, LectorsModule, MarksModule],
  exports: [MarksStudentsCoursesLectorsService],
  providers: [MarksStudentsCoursesLectorsService],
})
export class MarksStudentsCoursesLectorsModule {}
