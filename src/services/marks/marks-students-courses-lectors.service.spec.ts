import { Test, TestingModule } from '@nestjs/testing';
import { MarksStudentsCoursesLectorsService } from './marks-students-courses-lectors.service';

describe('MarksService', () => {
  let service: MarksStudentsCoursesLectorsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MarksStudentsCoursesLectorsService],
    }).compile();

    service = module.get<MarksStudentsCoursesLectorsService>(
      MarksStudentsCoursesLectorsService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
