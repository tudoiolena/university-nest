import {
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Mark } from './entities/mark.entity';
import { Repository } from 'typeorm';
import { AddMarkToStudentDto } from '../../controllers/marks/dto/add-mark-to-student.dto';
import { MarksResponseDto } from '../../controllers/marks/dto/marks-response.dto';
import { MarksByStudentIdResponseDto } from '../../controllers/marks/dto/marks-by-studentId-response.dto';
import { MarksByCourseIdResponseDto } from '../../controllers/marks/dto/marks-by-courseId-response.dto';
import { StudentsWithGroupNameResponseDto } from 'src/controllers/students/dto/students-with-group-name-response.dto';
import { CourseResponseDto } from '../../controllers/courses/dto/course-response.dto';
import { LectorWithCoursesResponseDto } from '../../controllers/lectors/dto/lector-with-courses-response.dto';

@Injectable()
export class MarksService {
  constructor(
    @InjectRepository(Mark)
    private readonly markRepository: Repository<Mark>,
  ) {}

  async getAllMarks(): Promise<MarksResponseDto[]> {
    const marks = await this.markRepository
      .createQueryBuilder('mark')
      .leftJoin('mark.course', 'course')
      .leftJoin('mark.student', 'student')
      .leftJoin('mark.lector', 'lector')
      .select([
        'mark.id as "markId"',
        'mark.createdAt as "markCreatedAt"',
        'mark.updatedAt as "markUpdatedAt"',
        'course.name as "courseName"',
        'lector.name as "lectorName"',
        'student.name as "studentName"',
        'mark.mark as mark',
      ])
      .getRawMany();

    if (marks.length === 0) {
      throw new NotFoundException('There are no marks yet');
    }

    return marks;
  }

  async getMarksByStudentId(
    studentId: string,
  ): Promise<MarksByStudentIdResponseDto[]> {
    const marks = await this.markRepository
      .createQueryBuilder('mark')
      .leftJoin('mark.student', 'student')
      .leftJoin('mark.course', 'course')
      .select(['course.name as "courseName"', 'mark.mark as mark'])
      .where('student.id = :id', { id: studentId })
      .getRawMany();

    if (marks.length === 0) {
      throw new NotFoundException(
        `There are no marks for ${studentId} student`,
      );
    }

    return marks;
  }

  async getMarksByCourseId(
    courseId: string,
  ): Promise<MarksByCourseIdResponseDto[]> {
    const marks = await this.markRepository
      .createQueryBuilder('mark')
      .leftJoin('mark.student', 'student')
      .leftJoin('mark.course', 'course')
      .leftJoin('mark.lector', 'lector')
      .select([
        'course.name as "courseName"',
        'lector.name as "lectorName"',
        'student.name as "studentName"',
        'mark.mark as mark',
      ])
      .where('course.id = :id', { id: courseId })
      .getRawMany();

    if (marks.length === 0) {
      throw new NotFoundException('There are no marks for this course');
    }

    return marks;
  }

  async checkExistingMark(
    studentId: string,
    addMarkToStudentDto: AddMarkToStudentDto,
  ): Promise<Mark> {
    const existingMark = await this.markRepository.findOne({
      where: {
        student: { id: studentId },
        course: { id: addMarkToStudentDto.courseId },
        lector: { id: addMarkToStudentDto.lectorId },
      },
    });

    if (existingMark) {
      throw new ConflictException(
        'Mark already exists for this student, course, and lector',
      );
    }
    return existingMark;
  }

  async addMarkToStudent(
    student: StudentsWithGroupNameResponseDto,
    course: CourseResponseDto,
    lector: LectorWithCoursesResponseDto,
    mark: string,
  ): Promise<Mark> {
    const newMark = this.markRepository.create({
      student,
      course,
      lector,
      mark,
    });
    return this.markRepository.save(newMark);
  }
}
