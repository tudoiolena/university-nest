import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCourseDto } from '../../controllers/courses/dto/create-course.dto';
import { UpdateCourseDto } from '../../controllers/courses/dto/update-course.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Course } from './entities/course.entity';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { CourseResponseDto } from '../../controllers/courses/dto/course-response.dto';
import { CourseResponseByLectorIdDto } from '../../controllers/courses/dto/course-response-by-lector-id.dto';
import { SortCourseDto } from '../../controllers/courses/dto/sort-course.dto';

@Injectable()
export class CoursesService {
  constructor(
    @InjectRepository(Course)
    private readonly courseRepository: Repository<Course>,
  ) {}

  createCourse(createCourseDto: CreateCourseDto): Promise<CourseResponseDto> {
    return this.courseRepository.save(createCourseDto);
  }

  async findAllCourses(): Promise<CourseResponseDto[]> {
    return await this.courseRepository.find({});
  }

  async findAllSort(sortCourseDto: SortCourseDto): Promise<Course[]> {
    let courses = [];

    if (sortCourseDto.name) {
      courses = await this.courseRepository.find({
        where: { name: sortCourseDto.name },
      });
    } else if (sortCourseDto.sortOrder) {
      sortCourseDto.sortField = 'name';
      courses = await this.courseRepository.find({
        order: {
          [sortCourseDto.sortField]: sortCourseDto.sortOrder,
        },
      });
    } else {
      courses = await this.courseRepository.find();
    }

    if (courses.length === 0) {
      throw new NotFoundException('There no courses yet');
    }
    return courses;
  }

  async findCourseById(id: string): Promise<CourseResponseDto> {
    const course = await this.courseRepository.findOneBy({
      id,
    });

    if (!course) {
      throw new NotFoundException(`There is no student with ${id} id`);
    }
    return course;
  }

  async findCourseWithLectors(id: string): Promise<Course> {
    const course = await this.courseRepository.findOne({
      where: { id },
      relations: ['lectors'],
    });

    if (!course) {
      throw new NotFoundException(`There is no course with ${id} id`);
    }
    return course;
  }

  async findCourseWithStudents(id: string): Promise<Course> {
    const course = await this.courseRepository.findOne({
      where: { id },
      relations: ['students'],
    });

    if (!course) {
      throw new NotFoundException(`There is no course with ${id} id`);
    }
    return course;
  }

  async updateCourse(
    id: string,
    updateCourseDto: UpdateCourseDto,
  ): Promise<UpdateResult> {
    const course = await this.courseRepository.findOneBy({
      id,
    });
    if (!course) {
      throw new NotFoundException(`Course with #${id} ID not found`);
    }
    return await this.courseRepository.update(id, updateCourseDto);
  }

  async removeCourse(id: string): Promise<DeleteResult> {
    const course = await this.courseRepository.findOneBy({
      id,
    });
    if (!course) {
      throw new NotFoundException(`Course with #${id} ID not found`);
    }
    return this.courseRepository.delete(id);
  }

  async getAllCoursesByLectorId(
    lectorId: string,
  ): Promise<CourseResponseByLectorIdDto[]> {
    const courses = await this.courseRepository
      .createQueryBuilder('course')
      .leftJoin('course.lectors', 'lector')
      .select([
        'course.name as "courseName"',
        'course.description as "courseDesciption"',
        'course.hours as "courseHours"',
        'course.id as "courseId"',
        'lector.name as "lectorName"',
      ])
      .where('lector.id = :id', { id: lectorId })
      .getRawMany();

    if (courses.length === 0) {
      throw new NotFoundException('There are no courses for this lector');
    }

    return courses;
  }
}
