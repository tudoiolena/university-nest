import { ConflictException, Injectable } from '@nestjs/common';
import { CoursesService } from '../courses/courses.service';
import { Course } from '../courses/entities/course.entity';
import { StudentsService } from './students.service';
import { StudentsResponseDto } from 'src/controllers/students/dto/students-response.dto';
import { Student } from './entities/student.entity';

@Injectable()
export class StudentsCoursesService {
  constructor(
    private readonly studentService: StudentsService,
    private readonly courseService: CoursesService,
  ) {}

  async addStudentToCourse(
    studentId: string,
    courseId: string,
  ): Promise<StudentsResponseDto> {
    const student = await this.studentService.findStudent(studentId);

    const course = await this.courseService.findCourseWithStudents(courseId);

    if (
      student.courses &&
      student.courses.some((existingCourse) => existingCourse.id === course.id)
    ) {
      throw new ConflictException('Student already exists on this course');
    }

    if (!student.courses) {
      student.courses = [];
    }

    if (!course.students) {
      course.students = [];
    }

    student.courses.push(course as Course);
    //course.students.push(student as Student);

    await this.studentService.createStudent(student);
    // await this.courseService.createCourse(course);

    return student;
  }
}
