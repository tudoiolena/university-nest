import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateStudentDto } from '../../controllers/students/dto/create-student.dto';
import { UpdateStudentDto } from '../../controllers/students/dto/update-student.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { Student } from './entities/student.entity';
import { StudentIdDto } from '../../controllers/students/dto/student-id.dto';
import { StudentsResponseDto } from '../../controllers/students/dto/students-response.dto';
import { StudentsWithGroupNameResponseDto } from '../../controllers/students/dto/students-with-group-name-response.dto';
import { SortStudentDto } from '../../controllers/students/dto/sort-student.dto';
import { Observable, from } from 'rxjs';
import { StudentWithCoursesResponseDto } from 'src/controllers/students/dto/student-with-courses-response.dto';

@Injectable()
export class StudentsService {
  constructor(
    @InjectRepository(Student)
    private readonly studentRepository: Repository<Student>,
  ) {}

  studentSelect = [
    'student.id as id',
    'student.name as name',
    'student.surname as surname',
    'student.email as email',
    'student.age as age',
    'student.imagePath as "imagePath"',
    'group.name as "groupName"',
    'student.createdAt as "createdAt"',
    'student.updatedAt as "updatedAt"',
    'courses.name as "courses"',
  ];

  async createStudent(
    createStudentDto: CreateStudentDto,
  ): Promise<StudentsResponseDto> {
    return await this.studentRepository.save(createStudentDto);
  }

  async findAllStudents(sortStudentDto: SortStudentDto): Promise<Student[]> {
    const { limit = 10, offset = 0 } = sortStudentDto;
    let students = [];
    if (sortStudentDto.name) {
      students = await this.studentRepository.find({
        where: { name: sortStudentDto.name },
        relations: ['courses', 'group'],
      });
    } else if (sortStudentDto.sortOrder) {
      sortStudentDto.sortField = 'name';
      students = await this.studentRepository.find({
        order: {
          [sortStudentDto.sortField]: sortStudentDto.sortOrder,
        },
        skip: offset,
        take: limit,
      });
    } else {
      students = await this.studentRepository.find({
        relations: ['courses', 'group'],
      });
    }

    if (students.length === 0) {
      throw new NotFoundException('There no students yet');
    }
    return students;
  }

  async findStudentById(id: string): Promise<StudentsWithGroupNameResponseDto> {
    const student = await this.studentRepository.findOne({
      where: { id },
      relations: ['courses', 'group'],
    });
    // const student = await this.studentRepository
    //   .createQueryBuilder('student')
    //   .select(this.studentSelect)
    //   .leftJoin('student.group', 'group')
    //   .leftJoin('student.courses', 'courses')
    //   .where(`student.id = :id`, { id })
    //   .getRawOne();

    if (!student) {
      throw new NotFoundException(`Student with ${id} ID not found`);
    }
    return student;
  }

  public async findStudent(id: string): Promise<StudentWithCoursesResponseDto> {
    const student = await this.studentRepository.findOne({
      where: { id },
      relations: ['courses'],
    });

    if (!student) {
      throw new NotFoundException(`Student with #${id} ID not found`);
    }
    return student;
  }

  async updateStudent(
    studentIdValidationDto: StudentIdDto,
    updateStudentDto: UpdateStudentDto,
  ): Promise<UpdateResult> {
    const student = await this.studentRepository.findOneBy({
      id: studentIdValidationDto.id,
    });

    if (!student) {
      throw new NotFoundException(
        `Student with #${studentIdValidationDto.id} ID not found`,
      );
    }
    return await this.studentRepository.update(
      studentIdValidationDto.id,
      updateStudentDto,
    );
  }

  updateOne(id: string, student: Student): Observable<any> {
    delete student.name;
    delete student.email;
    delete student.surname;
    delete student.age;
    delete student.groupId;
    return from(this.studentRepository.update(id, student));
  }

  async removeStudent(
    studentIdValidationDto: StudentIdDto,
  ): Promise<DeleteResult> {
    const student = await this.studentRepository.findOneBy({
      id: studentIdValidationDto.id,
    });

    if (!student) {
      throw new NotFoundException(
        `Student with #${studentIdValidationDto.id} ID not found`,
      );
    }
    return await this.studentRepository.delete(studentIdValidationDto.id);
  }
}
