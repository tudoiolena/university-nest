import {
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { CoreEntity } from '../../../application/entities/core.entity';
import { Group } from '../../groups/entities/group.entity';
import { Course } from '../../courses/entities/course.entity';
import { Mark } from '../../marks/entities/mark.entity';

@Entity({ name: 'students' })
@Index('idx_student_name', ['name'])
export class Student extends CoreEntity {
  @Column({
    type: 'varchar',
    nullable: false,
  })
  name: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  surname: string;

  @Column({
    type: 'varchar',
    nullable: false,
    unique: true,
  })
  email: string;

  @Column({
    type: 'numeric',
    nullable: false,
  })
  age: number;

  @Column({
    type: 'varchar',
    nullable: true,
    name: 'image_path',
  })
  imagePath: string;

  @Column({
    type: 'integer',
    nullable: true,
    name: 'group_id',
  })
  groupId: string;

  @ManyToOne(() => Group, (group) => group.students, {
    nullable: true,
    eager: false,
    onDelete: 'SET NULL',
  })
  @JoinColumn({ name: 'group_id' })
  group: Group;

  @ManyToMany(() => Course, (course) => course.students, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  @JoinTable({
    name: 'student_course',
    joinColumn: {
      name: 'student_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'course_id',
      referencedColumnName: 'id',
    },
  })
  courses?: Course[];

  @OneToMany(() => Mark, (mark) => mark.student, { cascade: ['remove'] })
  marks: Mark[];
}
