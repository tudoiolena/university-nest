import { Module } from '@nestjs/common';
import { CoursesModule } from '../courses/courses.module';
import { StudentsModule } from './students.module';
import { StudentsCoursesService } from './students-courses.service';

@Module({
  imports: [StudentsModule, CoursesModule],
  providers: [StudentsCoursesService],
  exports: [StudentsCoursesService],
})
export class StudentsCoursesModule {}
