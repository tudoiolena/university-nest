import { CoreEntity } from '../../../application/entities/core.entity';
import { Column, Entity, JoinColumn } from 'typeorm';

@Entity({ name: 'resettokens' })
export class ResetToken extends CoreEntity {
  @Column({
    type: 'varchar',
    nullable: false,
  })
  token: string;

  @JoinColumn({ name: 'lector_email', referencedColumnName: 'email' })
  @Column({ type: 'varchar' })
  lector_email: string;
}
