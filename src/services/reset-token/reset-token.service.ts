import { Injectable } from '@nestjs/common';
import crypto from 'crypto';

import { DeleteResult, Repository } from 'typeorm';
import { ResetToken } from './entities/reset-token.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { ResetPasswordWithTokenResponseDto } from '../../controllers/auth/dto/reset-password-with-token.response.dto';

@Injectable()
export class ResetTokenService {
  constructor(
    @InjectRepository(ResetToken)
    private readonly resetTokenRepository: Repository<ResetToken>,
  ) {}

  public async generateResetToken(
    useremail: string,
  ): Promise<ResetPasswordWithTokenResponseDto> {
    const existingToken = await this.resetTokenRepository.findOne({
      where: { lector_email: useremail },
    });

    if (existingToken) {
      await this.resetTokenRepository.remove(existingToken);
    }

    const token = crypto.randomBytes(32).toString('hex');

    const resetToken = await this.resetTokenRepository.create({
      token,
      lector_email: useremail,
    });
    return this.resetTokenRepository.save(resetToken);
  }

  public async getResetToken(token: string): Promise<ResetToken> {
    return this.resetTokenRepository.findOneBy({ token });
  }

  public async removeResetToken(token: string): Promise<DeleteResult> {
    return this.resetTokenRepository.delete({ token });
  }
}
