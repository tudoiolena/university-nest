import { Module } from '@nestjs/common';
import { ResetTokenService } from './reset-token.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ResetToken } from './entities/reset-token.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ResetToken])],
  providers: [ResetTokenService],
  exports: [ResetTokenService],
})
export class ResetTokenModule {}
