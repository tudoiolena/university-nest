import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddManyToOneRelationBetweenMarksAndLectorsTables1692723882010
  implements MigrationInterface
{
  name = 'AddManyToOneRelationBetweenMarksAndLectorsTables1692723882010';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "marks" ADD "lector_id" integer`);
    await queryRunner.query(
      `ALTER TABLE "marks" ADD CONSTRAINT "FK_69b5af3347a46bb74ccc3c6f65c" FOREIGN KEY ("lector_id") REFERENCES "lectors"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "marks" DROP CONSTRAINT "FK_69b5af3347a46bb74ccc3c6f65c"`,
    );
    await queryRunner.query(`ALTER TABLE "marks" DROP COLUMN "lector_id"`);
  }
}
