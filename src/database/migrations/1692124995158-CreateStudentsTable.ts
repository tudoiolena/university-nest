import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateStudentsTable1692124995158 implements MigrationInterface {
  name = 'CreateStudentsTable1692124995158';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            CREATE TABLE "students" (
            "id" SERIAL NOT NULL, 
            "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
            "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), 
            "name" character varying NOT NULL, "surname" character varying NOT NULL, 
            "email" character varying NOT NULL, "age" numeric NOT NULL, 
            "image_path" character varying, 
            "group_id" integer, 
            CONSTRAINT "UQ_25985d58c714a4a427ced57507b" UNIQUE ("email"), 
            CONSTRAINT "PK_7d7f07271ad4ce999880713f05e" PRIMARY KEY ("id"))
        `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "students"`);
  }
}
