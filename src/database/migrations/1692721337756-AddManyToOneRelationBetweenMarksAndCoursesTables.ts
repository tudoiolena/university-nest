import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddManyToOneRelationBetweenMarksAndCoursesTables1692721337756
  implements MigrationInterface
{
  name = 'AddManyToOneRelationBetweenMarksAndCoursesTables1692721337756';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "marks" ADD CONSTRAINT "FK_3e39a10631f1c639777a2b99cb8" FOREIGN KEY ("course_id") REFERENCES "courses"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "marks" DROP CONSTRAINT "FK_3e39a10631f1c639777a2b99cb8"`,
    );
  }
}
