import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddResetTokenTable1693518063655 implements MigrationInterface {
  name = 'AddResetTokenTable1693518063655';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "resettokens" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "token" character varying NOT NULL, "lector_email" character varying NOT NULL, CONSTRAINT "PK_a7389e45d8b741ec731092b6fe2" PRIMARY KEY ("id"))`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "resettokens"`);
  }
}
