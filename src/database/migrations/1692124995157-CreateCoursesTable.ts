import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateCoursesTable1692124995157 implements MigrationInterface {
  name = 'CreateCoursesTable1692124995157';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            CREATE TABLE "courses" (
            "id" SERIAL NOT NULL, 
            "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), 
            "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
             "name" character varying NOT NULL, "description" character varying NOT NULL, 
             "hours" numeric NOT NULL,
              CONSTRAINT "PK_3f70a487cc718ad8eda4e6d58c9" PRIMARY KEY ("id"))`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "courses"`);
  }
}
