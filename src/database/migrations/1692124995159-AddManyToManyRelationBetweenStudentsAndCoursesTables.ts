import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddManyToManyRelationBetweenStudentsAndCoursesTables1692124995159
  implements MigrationInterface
{
  name = 'AddManyToManyRelationBetweenStudentsAndCoursesTables1692124995159';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            CREATE TABLE "student_course" (
            "student_id" integer NOT NULL, 
            "course_id" integer NOT NULL, 
            CONSTRAINT "PK_ab3f4979286e908ef30bd8cb5ee" 
            PRIMARY KEY ("student_id", "course_id"))
        `);
    await queryRunner.query(
      `CREATE INDEX "IDX_decddeaaed256b357c8d296426" ON "student_course" ("student_id") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_0ee43ae3da1f7093cb1d4645b1" ON "student_course" ("course_id") `,
    );

    await queryRunner.query(`
            ALTER TABLE "student_course" 
            ADD CONSTRAINT "FK_decddeaaed256b357c8d2964260" 
            FOREIGN KEY ("student_id") REFERENCES "students"("id") 
            ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
    await queryRunner.query(`
            ALTER TABLE "student_course" 
            ADD CONSTRAINT "FK_0ee43ae3da1f7093cb1d4645b18" 
            FOREIGN KEY ("course_id") 
            REFERENCES "courses"("id") 
            ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "student_course" DROP CONSTRAINT "FK_0ee43ae3da1f7093cb1d4645b18"`,
    );
    await queryRunner.query(
      `ALTER TABLE "student_course" DROP CONSTRAINT "FK_decddeaaed256b357c8d2964260"`,
    );

    await queryRunner.query(
      `DROP INDEX "public"."IDX_0ee43ae3da1f7093cb1d4645b1"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_decddeaaed256b357c8d296426"`,
    );
    await queryRunner.query(`DROP TABLE "student_course"`);
  }
}
