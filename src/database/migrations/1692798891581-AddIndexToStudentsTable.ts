import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddIndexToStudentsTable1692798891581
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`CREATE INDEX idx_student_name ON students(name)`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP INDEX idx_student_name`);
  }
}
