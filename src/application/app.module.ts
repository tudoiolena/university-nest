import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppService } from './app.service';
import { GroupsModule } from '../services/groups/groups.module';
import { CoursesModule } from '../services/courses/courses.module';
import { StudentsModule } from '../services/students/students.module';
import { CoursesControllerModule } from '../controllers/courses/courses.controller.module';
import { GroupsControllerModule } from '../controllers/groups/groups.controller.module';
import { StudentsControllerModule } from '../controllers/students/students.controller.module';
import { LectorsControllerModule } from '../controllers/lectors/lectors.controller.module';
import { MarksControllerModule } from '../controllers/marks/marks.controller.module';
import { typeOrmAsyncConfig } from '../configs/database/typeorm-config';
import { ConfigModule } from '../configs/config.module';
import { AuthControllerModule } from '../controllers/auth/auth.controller.module';
import { AuthModule } from '../services/auth/auth.module';
import { ResetTokenModule } from '../services/reset-token/reset-token.module';
import { LectorsModule } from '../services/lectors/lectors.module';
import { LectorsCoursesModule } from '../services/lectors/lectors-courses.module';
import { MarksStudentsCoursesLectorsModule } from '../services/marks/marks-students-courses-lectors.module';
import { MailerModule } from '@nestjs-modules/mailer';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

@Module({
  imports: [
    TypeOrmModule.forRootAsync(typeOrmAsyncConfig),
    ConfigModule,
    GroupsModule,
    CoursesModule,
    StudentsModule,
    MarksStudentsCoursesLectorsModule,
    LectorsModule,
    LectorsCoursesModule,
    CoursesControllerModule,
    GroupsControllerModule,
    StudentsControllerModule,
    LectorsControllerModule,
    MarksControllerModule,
    MarksControllerModule,
    AuthControllerModule,
    AuthModule,
    ResetTokenModule,
    MailerModule.forRoot({
      transport: {
        host: 'smtp.gmail.com',
        auth: {
          user: 'nestjsproject@gmail.com',
          pass: 'nacpyggnxmkainiz', //use for Mac
          //pass: 'ddynjjxeeojgigaf' //use for Windows
        },
      },
    }),
    //  ServeStaticModule.forRoot({
    //  rootPath: join(__dirname, '..', 'uploads'),
    // serveRoot: 'uploads',
    // serveStaticOptions: {
    //   redirect: false,
    //   index: false,
    // },
    // }),
  ],
  providers: [AppService],
})
export class AppModule {}
