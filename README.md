# nestjs-example
- Implemented jwt auth using Nest.js docs (for FE logic) - https://docs.nestjs.com/security/authentication
- Configured connection to db
- Added Swagger docs for auth endpoints - Open http://localhost:{APP_PORT}/docs
- Implemented logic with reset password - https://blog.logrocket.com/implementing-secure-password-reset-node-js/
- Logic for reset password - send POST http://localhost:3010/auth/reset-password-request and receive a token -> then use http://localhost:3010/auth/reset-password endpoint to change user password (token should be passed to dto)
- Implemented guard

# What should be done? 
- Move all entities from express with all relations. In this repo added relation for student-course (many-to-many) - student can join multiple courses. Added relation student -group 
- Create controllers (CRUD), services, dtos... (Better to start from lectors entity) - Check Nest.js CLI (https://docs.nestjs.com/cli/usages) `nest generate res`
- Modify auth to use database instead of storing everything in memory. In our system lector should be an entity which we need to use for auth. It already has email and password fields 
- Password for lectors should be stored in database using some hash algorithm(should be done in the end)

# Note
- You can use another project structure for your projects. Better to check everything in Nest.js docs - https://docs.nestjs.com/
